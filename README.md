## Targets dependency checker for catkin workspace ##

This simple script allow you to run deep dependency test. It is based on assumption that all targets inside the CMakeLists.txt should be compilable individually if the dependencies are set correctly.
Script works as follow:
```
1) Generate Makefiles and extract all targets from it
2) Exclude some default targets which we are not interested in
3) For each target do:
     3a) make target and log outputs
     3b) store status (FAIL|RULE|SUCC) and compilation time into the global log
     3c) make clean
4) Generate various ordering of global log (based on compilation time, alphabetically or status based)
5) Let the developers fix their dependency problem
     
```

To run this test, move script dep.sh next to the build, devel and src folder, **remove devel, build folder** and run /dep.sh .
Note that RULE status mean that target was probably extracted badly from CMakeLists and the make command results in sentence: "There is no RULE to make target..."

```
Usage: ./dep.sh [-h] [-v] [target1 [target2 [...]]] 
    -h print this help and exit
    -v verbose output, show output of all make commands (but logging to files is preserved)
    target1..N make specified targets only. If not specified all targets are used.
```

###Questions? ###

http://answers.ros.org/question/194486/catkin-automatic-dependency-checker/#

### Why testing? ###

Do you use code on various PC? Let's consider following situation assuming you forgot to add dependency on generated targets:

* We have pc with one core only. Command catkin_make -j1 will passed because all nodes are compiled in order they are parsed by catkin_make command.
* You move your code to server with many (N) cores inside. Catkin_make will compile N targets at once which means that your target can be compiled before dependency is created.
* The main problem is that it is hardware dependent and thus such a bug is hard to find but easy to fix once it was found.

Observations:

* catkin_make -j1 **DOSN'T IMPLY** test will pass
* catkin_make -jX where X is lower than number of targets **DOSN'T IMPLY** test will pass
* catkin_make -jINF **SHOULD IMPLY** test will pass (but let's stay realistic :) )
* test will pass **IMPLY** catkin_make -jX will pass


###Known issues###

From what I realize up to now, this approach did not test if package itself is correctly set.
I mean that your package don't have to find all the libraries it requires if this library is founded in advance by another package.
This of course will produce an error if package will be in separate workspace but this test did not cover this types of mistakes yet.