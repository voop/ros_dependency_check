#!/bin/bash -i
#title        :dep.sh
#description  :This script will check dependencies for all targets inside the catkin_based workspace
#date         :October 2014
#version      :0.0.1
#usage        :./dep.sh -h
#author       :Vladimir Petrik, vladko.petrik@gmail.com
#========================================================================================================

usage() {
    echo "Usage: $0 [-h] [-v] [target1 [target2 [...]]] " 1>&2;
    echo "    -h print this help and exit"
    echo "    -v verbose output, show output of all make commands"
    echo "    target1..N make specified targets only. If not specified all targets are used."
    exit 1;
}

dir=`pwd`"/dep";
dir_devel="$dir/devel";
dir_build="$dir/build";
dir_logs="$dir/logs";
dir_src_from_build=`pwd`"/src";
verbose=0

while getopts ":hv" o; do
    case "${o}" in
        h)
            usage
            ;;
        v)
            verbose=1
            ;;
        \?) echo "Invalid option: -$OPTARG" >&2
            ;;
    esac
done
shift $(expr $OPTIND - 1)

selected_targets=( "${@:1}" )
exclude_targets=(cmake_force list_install_components edit_cache all doxygen run_tests Makefile clean_test_results test depend tests help preinstall cmake_check_build_system default_target gtest_main clean gtest install rebuild_cache)

rm -rf $dir 2> /dev/null
mkdir $dir
mkdir $dir_devel 
mkdir $dir_build 
mkdir $dir_logs

echo "########"
echo "Generating Makefiles, logging into: inital_cmake.log"
cd $dir_build
cmake $dir_src_from_build -DCATKIN_DEVEL_PREFIX=$dir_devel &> "$dir_logs/initial_cmake.log"

echo "Extracting targets"
targets=()
if [ ${#selected_targets[@]} -eq 0 ]; then
   targets=(`make -qp | awk -F':' '/^[a-zA-Z0-9][^$#\/\t=]*:([^=]|$)/ {split($1,A,/ /);for(i in A)print A[i]}'`)
   for del in ${exclude_targets[@]}; do
       targets=(${targets[@]/$del})
   done
else
   targets=( "${selected_targets[@]}" )
fi

echo "Going to test following targets:"
echo ${targets[@]}
echo "########"
echo ""

for t in "${targets[@]}"; do
    echo "Trying to build target $t, logging into $t.log"
    time_start=$(date +%s.%N)
    make_status=0;
    if [ $verbose -eq 1 ]; then
        make $t 2>&1 | tee "$dir_logs/$t.log"
        make_status=${PIPESTATUS[0]};
    else
        make $t &> "$dir_logs/$t.log"
        make_status=$?;
    fi
    time_end=$(date +%s.%N)
    time_diff=$(echo "$time_end - $time_start" | bc)
    msg=$time_diff","$t
    if [[ make_status -ne 0 ]] ; then
        no_rule_pattern="make: *** No rule to make target \`$t'.  Stop."
        if grep -Fq "$no_rule_pattern" "$dir_logs/$t.log" > /dev/null; then
            msg="RULE,"$msg
        else
            msg="FAIL,"$msg
        fi
    else
        msg="SUCC,"$msg
    fi
    echo $msg >> $dir_logs/all_status.log
    echo $msg

    make clean
    if [[ $? -ne 0 ]] ; then
        echo "Failed to clean"
        exit 1
    fi
done

echo "Generating sorted logs."
sort -t, -k+2 -r -n $dir_logs/all_status.log > $dir/all_sorted_time.log
sort -t, -k+1 $dir_logs/all_status.log > $dir/all_sorted_status.log
sort -t, -k+3 $dir_logs/all_status.log > $dir/all_sorted_name.log


